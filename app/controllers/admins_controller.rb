class AdminsController < ApplicationController
    
    before_action :set_admin, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_admin!

    
    def index
      authorize Admin
      @admins = Admin.all
    end
    
    def show
      authorize @admin
    end
    
    def edit
      authorize @admin
    end
    
    def new
      # byebug
      authorize Admin
      @admin = Admin.new
    end
    
    def create
      authorize Admin
      @admin = Admin.new(admin_params)

        respond_to do |format|
          if @admin.save
            format.html { redirect_to @admin, notice: 'Admin was successfully created.' }
            format.json { render :show, status: :created, location: @admin }
          else
            format.html { render :new }
            format.json { render json: @admin.errors, status: :unprocessable_entity }
          end
        end
    end

  def update
    authorize @admin
    
    respond_to do |format|
      if @admin.update(admin_params)
        
        
        format.html { redirect_to @admin, notice: 'Admin was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin }
      else
        format.html { render :edit }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  authorize @admin
    @admin.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_path, notice: 'Admin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

 
  
  private

    def set_admin
      @admin = Admin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_params
      params.require(:admin).permit(:firstname,:lastname,:username,:phone,:email,:picture,:password,:password_confirmation)
    end    
end
