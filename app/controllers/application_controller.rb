class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  include Pundit
  
  def pundit_user
    current_admin || current_employee || nil
  end
  
  rescue_from Pundit::NotAuthorizedError do 
    if admin_signed_in? or employee_signed_in?
      redirect_to dashboard_path, alert: 'You do not have access to this page'
    else
      redirect_to new_employee_session_path, alert: 'You do not have access to this page'
    end
  end
  
  # rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

 private

 def user_not_authorized(exception)
   policy_name = exception.policy.class.to_s.underscore

   flash[:notice] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
   redirect_to(request.referrer || root_path)
 end
 
 def after_sign_out_path_for(resource_or_scope)
    # root_path
    new_employee_session_path
  end
 
 protected
  # def after_sign_in_path_for(resource)
    # request.referrer || dashboard_path
  # end
 


end
