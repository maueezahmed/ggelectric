class ClientsController < ApplicationController
    before_action :set_client, only: [:show, :edit, :update, :destroy]

    
    def index
      authorize Client
      @clients = Client.all
    end
    
    def show
      authorize @client
    end
    
    def edit
      authorize @client
    end
    
    def new
      authorize Client
      @client = Client.new
    end
    
    def create
      authorize Client
      @client = Client.new(client_params)

        respond_to do |format|
          if @client.save
            format.html { redirect_to @client, notice: 'client was successfully created.' }
            format.json { render :show, status: :created, location: @client }
          else
            format.html { render :new }
            format.json { render json: @client.errors, status: :unprocessable_entity }
          end
        end
    end

  def update
    authorize @client
    
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  authorize @client
    @client.destroy
    respond_to do |format|
      format.html { redirect_to cities_url, notice: 'client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

 
  
  private

    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:notes,:logo,:name,:address,:poc,:phone,:email)
    end
end