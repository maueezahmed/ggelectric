class EmployeesController < ApplicationController
    
    before_action :set_employee, only: [:show, :edit, :update, :destroy]

    
    def index
      authorize Employee
      @employees = Employee.all
    end
    
    def show
      authorize @employee
    end
    
    def edit
      authorize @employee
    end
    
    def new
      # byebug
      authorize Employee
      @employee = Employee.new
    end
    
    def create
      authorize Employee
      @employee = Employee.new(employee_params)

        respond_to do |format|
          if @employee.save
            format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
            format.json { render :show, status: :created, location: @employee }
          else
            format.html { render :new }
            format.json { render json: @employee.errors, status: :unprocessable_entity }
          end
        end
    end

  def update
    authorize @employee
    
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @employee
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_path, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

 
  
  private

    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:picture,:firstname,:lastname,:username,:phone,:email,:pay,:dob,:address,:password,:password_confirmation,:current_password)
    end    
end
