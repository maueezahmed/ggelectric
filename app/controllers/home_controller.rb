class HomeController < ApplicationController
    # before_action :authenticate_employee!,
    
    def dashboard
        if( (not employee_signed_in?) and (not admin_signed_in?) )
            return redirect_to new_employee_session_path, notice: "You need to Sign In to Continue"
        else
            @jobs_statuses = Job.statuses
            @tasks_statuses = Task.statuses
            if admin_signed_in?
                @admins = Admin.all
                @jobs=Job.all
                @clients = Client.all
                @employees = Employee.all
                @tasks = Task.all
                
            elsif employee_signed_in?
                @tasks = Task.joins(:task_assigneds).where("task_assigneds.assignee_type" => "Employee","task_assigneds.assignee_id" => current_employee.id)
                @jobs = Job.joins(:employee_jobs).where('employee_jobs.employee_id' => current_employee.id)
            end
        end
        
    end
end
