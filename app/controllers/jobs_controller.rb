class JobsController < ApplicationController
    before_action :set_job, only: [:show, :edit, :update, :destroy]
    # before_action :authenticate_admin!
    # 	load_and_authorize_resource

    
    def index
      authorize Job
      @jobs = Job.all
    end
    
    def assign_clients
      authorize Job
      @job = Job.find(params[:job_id])
      # byebug
      clients_to_be_assigned = params[:clientjob][:client_ids]
      # check_all_created
      clients_to_be_assigned.each do |client|
        if(client.present? and @job)
          Clientjob.create!(client_id: client,job_id:@job.id)
        end
      end
      
      redirect_to @job
    end
    
    def assign_employees
      authorize Job
      @job = Job.find(params[:job_id])
      # byebug
      employees_to_be_assigned = params[:clientjob][:employee_ids]
      # check_all_created
      employees_to_be_assigned.each do |employee|
        if(employee.present? and @job)
          EmployeeJob.create!(employee_id: employee,job_id:@job.id)
        end
      end
      
      redirect_to @job
    end
    
    def show
      authorize @job
      clients = Clientjob.where(job_id: @job.id).pluck(:client_id)
      @tasks = @job.tasks
      @all_unassigned_client = Client.where.not(id: clients)
      @all_assigned_clients = Client.joins(:clientjobs).where('clientjobs.job_id' => @job.id)
      
      employees =  EmployeeJob.where(job_id: @job.id).pluck(:employee_id)
      @all_unassigned_employees = Employee.where.not(id: employees)
      @all_assigned_employees = Employee.joins(:employee_jobs).where('employee_jobs.job_id' => @job.id)
      
      # byebug
      
      
    end
    
    def edit
      authorize @job
    end
    
    def new
      authorize Job
      @job = Job.new
    end
    
    def create
      authorize Job
      @job = Job.new(job_params)

        respond_to do |format|
          if @job.save
            format.html { redirect_to @job, notice: 'job was successfully created.' }
            format.json { render :show, status: :created, location: @job }
          else
            format.html { render :new }
            format.json { render json: @job.errors, status: :unprocessable_entity }
          end
        end
  end

  def update
    authorize @job
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @job
    @job.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_path, notice: 'job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

 
  
  private

    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:picture,:name,:contact,:due_date,:status,:comment,:notes,:total_time)
    end
end

