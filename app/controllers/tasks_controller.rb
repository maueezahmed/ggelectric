class TasksController < ApplicationController
    
      	before_action :set_task, only: [:show, :edit, :update, :destroy]

    
    def index
      authorize Task
      @tasks = Task.all
    end
    
    def show
      authorize @task
      user = current_admin || current_employee
      # byebug
      @unassigned_employees =Employee.where.not(id: TaskAssigned.where(task_id: @task.id, assignee_type: "Employee").pluck(:assignee_id))
      @unassigned_admins =Admin.where.not(id: TaskAssigned.where(task_id: @task.id, assignee_type: "Admin").pluck(:assignee_id))
      @assigned_users = TaskAssigned.where(task_id: @task.id)
      
      
    end
    
    def status
      user = current_admin || current_employee
      
      @task = Task.find(params[:task_id])
      if(params[:status].present?)
        # byebug
        @task.status = params[:status].to_i
        @task.save!
        # byebug
      end
      
      # # byebug
      if @task.completed?
      #   # byebug
        job_tasks = @task.job.tasks.map &->(x){ x.completed? }
        # byebug
        if job_tasks.all?
          @task.job.complete!
        end
      elsif not @task.completed?
        @task.job.in_progress!
      end
      redirect_to @task
    end
    
    def edit
      authorize @task
    end
    
    def assign
      @task = Task.find(params[:task_id])
      
      admins = params[:admins]
      employees = params[:employees]
      
      if admins
        admins.each do |admin|
          exist = TaskAssigned.exists?(task_id: @task.id,assignee_type: "Admin",assignee_id: admin)
          if not exist
            TaskAssigned.create!(task_id: @task.id,assignee_type: "Admin",assignee_id: admin)
          end
        end
      end
      
      if employees
        employees.each do |emp|
          exist = TaskAssigned.exists?(task_id: @task.id,assignee_type: "Employee",assignee_id: emp)
          if not exist  
            TaskAssigned.create!(task_id: @task.id,assignee_type: "Employee",assignee_id: emp)
          end
        end
      end
      redirect_to task_path(@task)
    end
    
    def new
      
      authorize Task
      user = current_admin || current_employee
      @task = user.tasks.new
      # @task.job_id = @job.id
    end
    
    def create
      authorize Task
      user = current_admin || current_employee
      @task = user.tasks.new(task_params)
      # @task.job_id = @job.id

        respond_to do |format|
          if @task.save
            format.html { redirect_to task_url(@task), notice: 'Task was successfully created.' }
            format.json { render :show, status: :created, location: @task }
          else
            format.html { render :new }
            format.json { render json: @task.errors, status: :unprocessable_entity }
          end
        end
  end

  def update
    authorize @task
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

    def destroy
        authorize @task
        @task.destroy
        respond_to do |format|
            format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

 
  
    private

    def set_task
      
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:picture,:name,:content,:notes,:comment,:status,:job_id)
    end
    
end
