class Employee < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
         
  def name
    firstname + " " + lastname
  end
  
  has_many :employee_jobs, :dependent => :destroy
  has_many :tasks, as: :owner
  
  has_many :task_assigned, :as => :assignee, :dependent => :destroy
  
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/

  # has_many :tasks, as: :assignee
end
