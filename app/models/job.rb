class Job < ApplicationRecord
    enum status: [:in_progress,:complete]
    has_many :employee_jobs, :dependent => :destroy
    has_many :tasks, :dependent => :destroy
    
    has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
    
    
end
