class Task < ApplicationRecord
  belongs_to :job
  belongs_to :owner, polymorphic: true
  has_many :task_assigneds, :dependent => :destroy
  enum status: [:completed, :waiting_for_client,:waiting_for_vendor]
  
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
  
  def is_owner?(user)
    # user = current_admin || current_employee
    return owner == user
  end
  
  def is_assigned?(user)
    TaskAssigned.exists?(task_id: id, assignee_type: user.class.to_s, assignee_id: user.id) 
  end
end
