class TaskAssigned < ApplicationRecord
    belongs_to :assignee, polymorphic: true
end
