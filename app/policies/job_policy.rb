class JobPolicy < ApplicationPolicy
  
  def show?
    user.is_a? Admin or (user.is_a? Employee and EmployeeJob.exists?(job_id: record.id, employee_id: user.id))
  end
  
  def index?
    user.is_a? Admin
  end
  
  def update?
    user.is_a? Admin
  end
  
  def destroy?
    user.is_a? Admin
  end
  
  def new?
    user.is_a? Admin
  end
  
  def edit?
    user.is_a? Admin
  end
  
  def create?
    user.is_a? Admin
  end
  
  def assign_employees?
    user.is_a? Admin
  end
  
  def assign_clients?
    user.is_a? Admin
  end
end