class TaskPolicy < ApplicationPolicy
  
  def show?
    (user.is_a? Admin) || (user.is_a? Employee)
  end
  
  def index?
    (user.is_a? Admin) || (user.is_a? Employee)
  end
  
  def update?
    (user.is_a? Admin) || (record.is_owner?(user))
  end
  
  def destroy?
    (user.is_a? Admin) || (record.is_owner?(user))
  end
  
  def new?
    (user.is_a? Admin) || (user.is_a? Employee)
  end
  
  def edit?
    (user.is_a? Admin) || (record.is_owner?(user))
  end
  
  def create?
    (user.is_a? Admin) || (user.is_a? Employee)
  end
  
  def status?
    (user.is_a? Admin) || (record.is_assigned?(user))
  end
  
end