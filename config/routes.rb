Rails.application.routes.draw do
  devise_for :employees
  devise_for :admins
  
  # devise_scope :employee do
  #   root to: "devise/sessions#new"
  # end
  
  root to: "home#dashboard"
  
  authenticated :admin do
    root "home#dashboard", as: :admin_dashboard
  end
  
  authenticated :employee do
    root "home#dashboard", as: :employee_dashboard
  end
  
  get 'dashboard', to: 'home#dashboard',as: :dashboard
  resources :jobs, except:[:index] do
    
    get 'assign_clients', to: 'jobs#assign_clients',as: :assign_clients
    get 'assign_employees', to: 'jobs#assign_employees',as: :assign_employees
  end
  resources :tasks , except:[:index] do
    get 'assign', to: 'tasks#assign',as: :assign
    get 'status', to: 'tasks#status',as: :status
  end
  
  resources :admins , except:[:index]
  resources :employees , except:[:index]
  resources :clients, except:[:index]
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
