class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs do |t|
      t.string :name
      t.string :contact
      t.date :due_date
      t.integer :status
      t.string :comment
      t.text :notes
      t.integer :total_time

      t.timestamps
    end
  end
end
