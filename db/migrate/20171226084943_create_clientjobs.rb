class CreateClientjobs < ActiveRecord::Migration[5.1]
  def change
    create_table :clientjobs do |t|
      t.references :client, foreign_key: true
      t.references :job, foreign_key: true

      t.timestamps
    end
  end
end
