class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :content
      t.text :notes
      t.string :comment
      t.integer :status
      t.references :job, foreign_key: true
      t.references :owner, polymorphic: true, index: true

      t.timestamps
    end
  end
end
