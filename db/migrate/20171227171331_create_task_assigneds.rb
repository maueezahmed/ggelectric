class CreateTaskAssigneds < ActiveRecord::Migration[5.1]
  def change
    create_table :task_assigneds do |t|
      t.references :assignee, polymorphic: true, index: true
      t.references :task, index: true

      t.timestamps
    end
  end
end
