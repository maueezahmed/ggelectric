class AddAttachmentPictureToEmployees < ActiveRecord::Migration[5.1]
  def self.up
    change_table :employees do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :employees, :picture
  end
end
