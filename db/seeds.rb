# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Admin.create!(firstname: "Admin", lastname:"Admin",username:"admin1",email:"17100283@lums.edu.pk",password:"admin123")

# Employee.create!(firstname: "John", lastname:"Watson",username:"admin",email:"employee@test.com",password:"admin123")
# Employee.create!(firstname: "Sherlock", lastname:"Holmes",username:"admin2",email:"employee2@test.com",password:"admin123")